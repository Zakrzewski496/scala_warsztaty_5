import java.io.{FileNotFoundException, IOException}

import org.scalatest.FeatureSpec

import scala.io.Source
import scala.util.control.Exception.catching

class ExceptionSpec extends FeatureSpec {

  feature("Show how try/catch works") {
    scenario("Try/catch with good file") {
      val fileName = "xd"

      val content =
        try {
          Source.fromFile(fileName).getLines().mkString("\n")
        } catch {
          case e: FileNotFoundException => throw new SystemError(e)
          case e: IOException => throw new SystemError(e)
        } finally {
          println("Finally...")
        }

      assert(content != null)
      assert(!content.isEmpty)
    }
    scenario("Try/catch with wrong file") {
      val fileName = "xD"

      assertThrows[FileNotFoundException](Source.fromFile(fileName).getLines().mkString("\n"))
    }
  }

  feature("Show how Catch class works") {
    scenario("Catch class with good file") {
      val fileName = "xd"

      val fileCatch = catching(classOf[FileNotFoundException], classOf[IOException]).withApply(e => throw new SystemError(e)).andFinally(println("Finally..."))

      val content = fileCatch{Source.fromFile(fileName).getLines().mkString("\n")}

      val content2 = fileCatch.apply(Source.fromFile(fileName).getLines().mkString("\n"))

      assert(content != null)
      assert(content2 != null)
      assert(content.equals(content2))
    }
  }

  feature("Show opt in Catch class") {
    scenario("Catch class with wrong file") {
      val fileName = "xD"

      val fileCatch = catching(classOf[FileNotFoundException], classOf[IOException]).withApply(e => throw new SystemError(e)).andFinally(println("Finally..."))

      val content = fileCatch.opt{Source.fromFile(fileName).getLines().mkString("\n")}

      println(content)

      println(content.getClass)

      assert(content != null)

      assert(content.isEmpty)

      assert(content.getOrElse("Cannot found file").equals("Cannot found file"))
    }
  }

  feature("Show either in Catch class") {
    scenario("Catch class with wrong file") {
      val fileName = "xD"

      val fileCatch = catching(classOf[FileNotFoundException], classOf[IOException]).withApply(e => throw new SystemError(e)).andFinally(println("Finally..."))

      val content = fileCatch.either{Source.fromFile(fileName).getLines().mkString("\n")}

      println(content)

      println(content.getClass)

      assert(content != null)

      assert(content.isLeft)

      assert(!content.isRight)

      assert(content.right.getOrElse("Cannot found file").equals("Cannot found file"))
    }
  }

}
