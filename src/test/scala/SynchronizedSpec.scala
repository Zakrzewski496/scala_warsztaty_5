import org.scalatest.FeatureSpec

import scala.collection.mutable

class SynchronizedSpec extends FeatureSpec {

  case class User(name: String, id: Int)

  class InvertedIndex(val userMap: mutable.Map[String, User]) {
    def this() = this(new mutable.HashMap[String, User]())

    def tokenizeName(name: String): Seq[String] = {
      name.split(" ").map(_.toLowerCase)
    }

    def add(term: String, user: User) {
      userMap += term -> user
    }

    // Metoda nie jest zabezpieczona - przy wielu klientach może dojść do niespójności danych
//    def add(user: User) {
//      tokenizeName(user.name).foreach { term =>
//        add(term, user)
//      }
//    }

    // Metoda również posiada wadę - wywołanie metody tokenizeName może być umieszczona poza synchronized - oszczędność czasu
//    def add(user: User){
//      userMap.synchronized {
//        tokenizeName(user.name).foreach {
//          term => add(term, user)
//        }
//      }
//    }

    def add(user: User) {
      val tokens = tokenizeName(user.name)

      tokens.foreach { term =>
        userMap.synchronized {
          add(term, user)
        }
      }
    }
  }

}
