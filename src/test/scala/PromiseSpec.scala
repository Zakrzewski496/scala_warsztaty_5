import org.scalatest.FeatureSpec

import scala.concurrent.{Future, Promise}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class PromiseSpec extends FeatureSpec {

  case class TaxCut(reduction: Int)

  case class Excuse(msg: String) extends Exception(msg)

  feature("Show promises") {
    scenario("Reduce taxes") {

      val prom = Promise[String]()
      prom.success("First complete")
//      prom.success("Second complete")



      def redeemCampaignPledge(): Future[TaxCut] = {
        val p = Promise[TaxCut]()
        Future {
          println("Starting...")
          Thread.sleep(20)
          p.success(TaxCut(20))
          println("Reduced taxes!")
        }
        p.future
      }

      def breakingPromises(): Future[TaxCut] = {
        val p = Promise[TaxCut]()
        Future {
          println("Starting breaking promises...")
          Thread.sleep(20)
          p.failure(Excuse("CRISIS. CANNOT REDUCE TAXES"))
          println("Taxes not reduced")
        }
        p.future
      }

      println("Checking promises...")
      val taxCutF: Future[TaxCut] = redeemCampaignPledge()
      taxCutF.onComplete {
        case Success(TaxCut(reduction)) =>
          assert(taxCutF.isCompleted)
          println("Taxes cut")
        case Failure(ex) =>
          println("Taxes not cut")
      }


//      val brokenPromise: Future[TaxCut] = breakingPromises()
//      brokenPromise.onComplete {
//        case Success(TaxCut(reduction)) =>
//          assert(brokenPromise.isCompleted)
//          println("Promises are broken")
//        case Failure(ex) =>
//          println(ex.getMessage)
//      }
    }

  }




}
