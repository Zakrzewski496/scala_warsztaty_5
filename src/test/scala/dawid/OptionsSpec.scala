package dawid

import org.scalatest.FeatureSpec

class OptionsSpec extends FeatureSpec {
  def toInt(in: String): Option[Int] = {
    try {
      Some(Integer.parseInt(in.trim))
    } catch {
      case _: NumberFormatException => None
    }
  }

  feature("Show how Options work") {

    val someResult: Option[Int] = toInt("100")
    val noneResult: Option[Int] = toInt("one hundred")
    val noneStr = "That didn't work."

    scenario("Option with match case") {
      someResult match {
        case Some(i) => assert(i.equals(100))
      }
      noneResult match {
        case None => assert(true)
      }
    }

    scenario("Option with getOrElse method") {
      assertResult(100) {
        someResult.getOrElse(noneStr)
      }
      assertResult(noneStr) {
        noneResult.getOrElse(noneStr)
      }
    }

    scenario("Option with getOrElse and map methods") {
      val str: String = "example string"
      val optionStr: Option[String] = Some(str)

      assertResult(str.toUpperCase) {
        optionStr map {
          _.trim
        } filter {
          _.length != 0
        } map {
          _.toUpperCase
        } getOrElse noneStr
      }
      assertResult(noneStr) {
        Option("") map {
          _.trim
        } filter {
          _.length != 0
        } map {
          _.toUpperCase
        } getOrElse noneStr
      }
    }

    scenario("Option with flatMap method") {
      val bag = List("1", "2", "foo", "3", "bar")
      assertResult(6) {
        bag.flatMap(toInt).sum
      }
    }
  }
}
