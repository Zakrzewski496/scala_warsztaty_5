package dawid

import org.scalatest.FeatureSpec

class StringInterpolationSpec extends FeatureSpec {

  feature("Show how s interpolation works") {
    scenario("S interpolation examples") {
      val name = "James"
      val age = 33

      assert(s"Hello, $name".equals("Hello, James"))
      assert(s"${1 + 1}".equals("2"))
      assert(s"You are 33 years old: ${age == 33}".equals("You are 33 years old: true"))
    }
  }

  feature("Show how f interpolation works") {
    scenario("F interpolation examples") {
      val name = "James"
      val height = 1.9d

      assert(f"$name%s is $height%2.2f meters tall".equals("James is 1,90 meters tall")) // Usage of %f
      assert(f"$name%S".equals(name.toUpperCase())) // %S - upper case
      assert(f"$height".equals("1.9")) // Takes height as String, because there is no formatter.
      assert("%s is %2.2f meters tall".format(name, height).equals("James is 1,90 meters tall")) // f interpolator usage alternative

      //println(f"$height%4d")  // Will return Error of type missmatch. Even IntelliJ highlights warning
    }
  }

  feature("Show how raw interpolation works") {
    scenario("raw interpolation examples") {
      assert(s"a\nb".equals("a\nb")) // s interpolation breaks line because of \n
      assert(raw"a\nb".equals("a\\nb")) // raw interpolation takes string as it is
      assert("""hello \n World""".equals("hello \\n World")) // """ works exactly like raw
    }
  }
}
