package dawid

import org.scalatest.FeatureSpec

class EitherSpec extends FeatureSpec {
  val divError = "Dude, can't divide by 0"
  val parseErrorMsg = "Cannot parse string into integer"

  def divideXByY(x: Int, y: Int): Either[String, Int] = {
    if (y == 0) Left(divError)
    else Right(x / y)
  }

  def toIntWithEither(in: String): Either[String, Int] = {
    try {
      Right(Integer.parseInt(in.trim))
    } catch {
      case _: NumberFormatException => Left("Cannot parse string into integer")
    }
  }

  feature("Show how Either work") {
    scenario("Divide x by y method with Either") {
      assertResult(1) {
        divideXByY(1, 1) match {
          case Left(s) => s
          case Right(i) => i
        }
      }
      assertResult(divError) {
        divideXByY(1, 0) match {
          case Left(s) => s
          case Right(i) => i
        }
      }
    }

    scenario("toInt method with Either") {
      assertResult(100) {
        toIntWithEither("100") match {
          case Right(x) => x
          case Left(x) => x
        }
      }
      assertResult(parseErrorMsg) {
        toIntWithEither("text") match {
          case Right(x) => x
          case Left(x) => x
        }
      }
    }
  }
}
