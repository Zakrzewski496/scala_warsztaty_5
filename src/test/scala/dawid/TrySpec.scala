package dawid

import org.scalatest.FeatureSpec

import scala.io.Source
import scala.util.{Failure, Success, Try}

class TrySpec extends FeatureSpec {
  def readTextFile(filename: String): Try[List[String]] = {
    Try(Source.fromFile(filename).getLines.toList)
  }

  def toIntWithTry(in: String): Try[Int] = {
    Try(Integer.parseInt(in.trim))
  }

  feature("Show how Try works") {
    scenario("Make use of Failure. Reading text from file that doesn't exist") {
      val filename = "filet.txt"

      assertResult("java.io.FileNotFoundException: filet.txt (Nie można odnaleźć określonego pliku)") {
        readTextFile(filename) match {
          case Success(lines) => lines.mkString("\n")
          case Failure(f) => s"$f"
        }
      }
    }

    scenario("Make use of Success. Successfully reading from existing file") {
      val filename = "file.txt"

      assertResult("first\nsecond\nthird") {
        readTextFile(filename) match {
          case Success(lines) => lines.mkString("\n")
          case Failure(f) => s"$f"
        }
      }
    }

    scenario("Success while parsing string to int") {
      assertResult(100) {
        toIntWithTry("100") match {
          case Success(i) => i
          case Failure(f) => f
        }
      }
    }

    scenario("Failure while parsing string to int") {
      assertResult("java.lang.NumberFormatException: For input string: \"text\"") {
        toIntWithTry("text") match {
          case Success(i) => i
          case Failure(f) => s"$f"
        }
      }
    }
  }
}
