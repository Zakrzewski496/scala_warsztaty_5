import org.scalatest.FeatureSpec

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Random, Success, Try}


class FutureSpec extends FeatureSpec {

  type CoffeeBeans = String
  type GroundCoffee = String
  case class Water(temperature: Int)
  type Milk = String
  type FrothedMilk = String
  type Espresso = String
  type Cappuccino = String



  case class GrindingException(msg: String) extends Exception(msg)
  case class FrothingException(msg: String) extends Exception(msg)
  case class WaterBoilingException(msg: String) extends Exception(msg)
  case class BrewingException(msg: String) extends Exception(msg)



  def grindSequentially(beans: CoffeeBeans): GroundCoffee = s"ground coffee of $beans"
  def heatWaterSequentially(water: Water): Water = water.copy(temperature = 85)
  def frothMilkSequentially(milk: Milk): FrothedMilk = s"frothed $milk"
  def brewSequentially(coffee: GroundCoffee, heatedWater: Water): Espresso = "espresso"
  def combine(espresso: Espresso, frothedMilk: FrothedMilk): Cappuccino = "cappuccino"


  def grind(beans: CoffeeBeans): Future[GroundCoffee] = Future {
    println("start grinding..." + " Time: " + System.currentTimeMillis())
    Thread.sleep(Random.nextInt(10))
    if (beans == "baked beans") throw GrindingException("are you joking?")
    println("finished grinding..." + " Time: " + System.currentTimeMillis())
    s"ground coffee of $beans"
  }

  def heatWater(water: Water): Future[Water] = Future {
    println("heating the water now" + " Time: " + System.currentTimeMillis())
    Thread.sleep(Random.nextInt(10))
    println("hot, it's hot!" + " Time: " + System.currentTimeMillis())
    water.copy(temperature = 85)
  }

  def frothMilk(milk: Milk): Future[FrothedMilk] = Future {
    println("milk frothing system engaged!" + " Time: " + System.currentTimeMillis())
    Thread.sleep(Random.nextInt(10))
    println("shutting down milk frothing system" + " Time: " + System.currentTimeMillis())
    s"frothed $milk"
  }

  def brew(coffee: GroundCoffee, heatedWater: Water): Future[Espresso] = Future {
    println("happy brewing :)" + " Time: " + System.currentTimeMillis())
    Thread.sleep(Random.nextInt(10))
    println("it's brewed!" + " Time: " + System.currentTimeMillis())
    "espresso"
  }


  def prepareCappuccino(): Try[Cappuccino] = for {
    ground <- Try(grindSequentially("arabica beans"))
    water <- Try(heatWaterSequentially(Water(25)))
    espresso <- Try(brewSequentially(ground, water))
    foam <- Try(frothMilkSequentially("milk"))
  } yield combine(espresso, foam)

  def prepareCappuccinoConcurrently(): Future[Cappuccino] = {
    val groundCoffee = grind("arabica beans")
    val heatedWater = heatWater(Water(20))
    val frothedMilk = frothMilk("milk")
    for {
      ground <- groundCoffee
      water <- heatedWater
      foam <- frothedMilk
      espresso <- brew(ground, water)
    } yield combine(espresso, foam)
  }


  feature("Show how sequentially works") {
    scenario("Sequentially ordered") {

      val result = prepareCappuccino()

      println(result)

      assert(result != null)
      assert(result.isSuccess)
      assert(result.get.equals("cappuccino"))
    }
  }

  feature("Show how concurrently works") {
    scenario("Concurrently ordered") {
      val temperatureOkay: Future[Boolean] = heatWater(Water(25)).map { water =>
        println("we're in the future!")
        (80 to 85).contains(water.temperature)
      }

      temperatureOkay.onComplete({
        case Success(result) =>
          assert(temperatureOkay.isCompleted)
          assert(temperatureOkay.value.get.get)
      })

      assert(!temperatureOkay.isCompleted)

      val cappuccino = prepareCappuccinoConcurrently()


      cappuccino.onComplete({
        case Success(result) =>
          assert(cappuccino.isCompleted)
      })

      assert(!cappuccino.isCompleted)

    }
  }

}
