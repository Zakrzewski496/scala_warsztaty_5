object OwnStringInterpolator {

  implicit class CounterSC(val sc: StringContext) extends AnyVal {
    // Define functions that we want to use with string interpolation syntax
    def partsCount(args: Any*): Int = sc.parts.length

    def argsCount(args: Any*): Int = args.length

    def tokenCount(args: Any*): Int = sc.parts.length + args.length

    def getParts(args: Any*): String =
      s"parts: ${sc.parts.mkString(",")}"

    def getArgs(args: Any*): String =
      s"args: ${args.mkString(",")}"
  }

  def main(args: Array[String]): Unit = {
    val name = "James"
    val height = 1.9d

    // Use the functions that we created when the implicit class is in scope
    val i = tokenCount"$name is $height meters tall"
    val p = partsCount"$name is $height meters tall"
    val a = argsCount"$name is $height meters tall"
    val parts = getParts"$name is $height meters tall"
    val arg = getArgs"$name is $height meters tall"

    println(s"token count: $i") // token count: 5
    println(s"args count: $a") // args count: 2
    println(arg) // args: James,1.9
    println(s"parts count: $p") // parts count: 3
    println(parts) // , is , meters tall
  }
}
